const process = require('process');
const { isIBAN } = require('validator');
if (process.argv.length !== 3 || !process.argv[2]) {
    console.log("yarn run iban YOUR_IBAN_VALUE\n");
}
const [ , , value ] = process.argv;
console.log(`IBAN: ${value} is ${isIBAN(value) ? 'valid' : 'invalid'}\n`);